# 引言
點字其實好多人都見過或摸過，但能理解到的人少之又少，更甚其實不同地方文化的點字都各有不同，最主要是點字只是一種形式，點字的表達方式需透過各自的翻譯使當地文字變成不同組合的點字。由於香港的母語不同於中國的普通話，我們使用的是粵語，所以香港使用的點字亦有所不同。有趣的是這個原因下，逐漸地被受迫害的粵語有著不常於一般文字的方式保存著。同時如果了解到這種本土點字，用一種簡單的方式就做到點字的話，視障人士與健視人士之間的資訊傳遞的速度就更快一步，「人人都能寫出具香港特色的文字予視障人士欣賞及交流」就是本計畫的目的。

# Introduction
Many people see or touch braille but not understand, there are different kinds of braille base on different language.  Because the native language of Hong Kong is cantonese, so braille in here is different with others, and it is a type to conserve cantonese ironically under some pressure. Besides, if we want to make more people understand the cantonese braille, it should be more convenience to translate the words. So the aim of this project is ‘everyone could write something which communicate to blind with Hong Kongese culture.

# 製作人
* 林潤豪
* Mr.A

# Creater
* LAM Yun Ho
* Mr.A

# 香港粵語
中華地區及外國華僑的母語多為中文或稱漢語。中文有多個不同的地道版本，可粗略分為十種，包括：
* 官語
* 吳語
* 粵語
* 閩語
* 晉語
* 湘語
* 客家話
* 贛語
* 徽語
* 平話

當中不同的地區因為環境、文化和歷史因素，衍生出各自特殊的口音和用詞。
香港的母語為香港粵語，這語言發展史可追溯至超過二千年前，經過多年改變和發展，在公元前220年的漢代建構稚形。粵語在漢語系統中較為複雜，原因是粵語有九聲六調，比大部分的漢語分支，甚至其他語言為多。此語言的詞彙數量是全球第二多，僅次於英語，可見其變化相當高。
粵語相當值得保育，除了因為它的長歷史性、高可變性、多詞彙性之外，它的廣泛性亦相當大。根據1996年美國國際語言暑期學院的估計，全球使用粵語的人數約有六千六百萬人，比當時法國總人口還要多。

# Cantonese
Because there are various environment, culture and history in China, it divided into different types of chinese, and they have different pronunciation and vocabulary. The native language of Hong Kong is cantonese, it developed and changed for over 2000 years. Cantonese has nine tones, and it has many vocabulary which is the second most in the world. Cantonese has worth to conservation, since it has long history, high variability, multi-vocabulary and widely used.

# 拼音
拼音的功用主要是以特定的標註使用者們能正確讀出字的指定發音，早十幾年的漢語字典多會用中文標註作拼音，近年可能由於外部文化侵入，多數出現英文字母組成的拼音。漢語的拼音主要由三個成份組成：聲母、韻母和聲調，其中有小部分文字只有韻母和聲調。
由於粵語的發音與其他漢語支派有分別，所以粵語亦有很大的分別。
現代組成粵語的共有19個聲母 (b、p、m、f、d、t、n、l、g、k、ng、h、z、c、s、gw、kw、j、w) ；
51個韻母 (aa、aai、aau、aam、aan、aang、aap、aat、aak、ai、au、am、an、ang、ap、at、ak、e、ei、eng、ek、i、iu、im、in、ing、ip、it、ik、o、oi、ou、on、ong、ot、ok、oe、oeng、oek、eoi、eon、eot、u、ui、un、ung、ut、uk、yu、yun、yut) ； 9個聲調(陰平、陰上、陰去、陽平、陽上、陽去、陰入、中入、陽入)。
理論上這些聲母、韻母和聲調共可組成8721個組合，同時由於有些字可以沒有聲母，所以總組合可達9180。但我們用中文大學網上的林語堂粵文字庫中，發現註音中只會有六調，而該字庫中約17068字中只有當中1885個發音，即是我們常用的粵語中有7295個發音未被使用，佔約79%。當中有多個文字擁有多於一個發音，另外有多個文字沒有同音字。

# Pinyin
Pinyin divided three parts in Cantonese, included initial, final and tone, but some of words have final and tone only. Cantonese has 19 initials, 51 finals and 9 tones, and it would be 9180 combines. However, there are only 1885 pronunciations in our table which is 21% only of all combines.

# 點字
在1821年由法國人路易‧布萊葉發明點字，又稱盲文或凸字，主要是讓視障人士以觸感閱讀。一個點字由六個單位組成，分布為左右兩行，上中下三層。根據六個單元中凸出或不凸出形成64種可能性，盲文在某幾通用表音文字中都有屬於每個字母的表示法。但由於語素文字沒有字母，所以其中之一的漢語會用拼音點字組合表示。而粵語點字所需的點字單元共79個，有關點字顯示可參考附件一。

# Braille
Louis Braille create braille for blind in 1821, there are six parts in every single braille. According to each part whether protrude or not, it would be 64 possible forms of braille. Since there are no alphabets in Chinese, it would be combined by pinyin.

# 製作過程
## 第一步：建立字庫
我們首先要建立所需字庫，香港粵語字庫比較有權威的網頁應該是香港中文大學公開給大眾使用的粵文網站。我們從此網中取下約37000列文字資料，再人手用十天時間去處理一字多音的問題，即把多音的中文字選定最常使用的一個音去代表，最後剩下17068字。當然用一個音去代表一個字是有問題的，例如「使用」的「使」和「大使館」的是同字不同音，可是連 Apple Inc. 和 Google 兩大公司的語音系統也處理不了，我們也自然是捨難取易地放棄找尋更佳方法。
與此同時，我們要將各個文字都配上它獨有的萬國碼。要注意的是一般的萬國碼是以十六進制標示，而在 FontForge 內是以十進制編號，所以記緊要改。

## 第二步：訂立標準點字結構
不同語言也有各自的點字顯示，但每個單元的點字也有一個既定的標準。

點字標準(1單元)
<svg version="1.1" width="685" height="1000" xmlns="http://www.w3.org/2000/svg">
    <circle cx="215" cy="250" r="75" fill="#000000" stroke="#000000"/>
    <circle cx="215" cy="500" r="75" fill="#000000" stroke="#000000"/>
    <circle cx="215" cy="750" r="75" fill="#000000" stroke="#000000"/>
    <circle cx="465" cy="250" r="75" fill="#000000" stroke="#000000"/>
    <circle cx="465" cy="500" r="75" fill="#000000" stroke="#000000"/>
    <circle cx="465" cy="750" r="75" fill="#000000" stroke="#000000"/>
</svg>

而一個中文字是由三個單元的點字組成，因此一完整中文字的標準為以下樣子。

點字標準(1完整中文字)
<svg version="1.1" width="2055" height="1000" xmlns="http://www.w3.org/2000/svg">
    <circle cx="215" cy="250" r="75" fill="#000000" stroke="#000000"/>
    <circle cx="215" cy="500" r="75" fill="#000000" stroke="#000000"/>
    <circle cx="215" cy="750" r="75" fill="#000000" stroke="#000000"/>
    <circle cx="465" cy="250" r="75" fill="#000000" stroke="#000000"/>
    <circle cx="465" cy="500" r="75" fill="#000000" stroke="#000000"/>
    <circle cx="465" cy="750" r="75" fill="#000000" stroke="#000000"/>
    <circle cx="900" cy="250" r="75" fill="#000000" stroke="#000000"/>
    <circle cx="900" cy="500" r="75" fill="#000000" stroke="#000000"/>
    <circle cx="900" cy="750" r="75" fill="#000000" stroke="#000000"/>
    <circle cx="1150" cy="250" r="75" fill="#000000" stroke="#000000"/>
    <circle cx="1150" cy="500" r="75" fill="#000000" stroke="#000000"/>
    <circle cx="1150" cy="750" r="75" fill="#000000" stroke="#000000"/>
    <circle cx="1585" cy="250" r="75" fill="#000000" stroke="#000000"/>
    <circle cx="1585" cy="500" r="75" fill="#000000" stroke="#000000"/>
    <circle cx="1585" cy="750" r="75" fill="#000000" stroke="#000000"/>
    <circle cx="1835" cy="250" r="75" fill="#000000" stroke="#000000"/>
    <circle cx="1835" cy="500" r="75" fill="#000000" stroke="#000000"/>
    <circle cx="1835" cy="750" r="75" fill="#000000" stroke="#000000"/>
</svg>

由於字體工具 FontForge 的圖像坐標表示有不同，在 FontForge 下的一完整中文字的標準可參考 src/dot-data.csv。

## 第三步：製出各種單元的圖像標準
粵語點字由19個聲母加51個韻母加9個聲調，共79個圖像根據特定語言系統組成，如果要用電腦把組合合成，必須要將該79個圖像以 FontForge 的座標編寫方式定義好，詳見 src/table-data.csv。

## 第四步：認識 FontForge 結構

## 第五步：產生及測試
完成自動編寫後，只要將檔案用 FontForge 開啟，然後按「產生」，選擇「.otf」類型。
把產生的otf檔案安裝，我們再隨機選取10個中文字去核對，證實無誤後便可以算第一版本完成。

# Process
## First: create a table
We collected about 37000 words data from the Chinese University of Hong Kong website, and then we filter those mult-pronunciations words by man-hand for ten days, which reduces the data in table from over 37000 to 17068. Of cause, it is a problem that use one pronunciation to replace all other pronunciations of a word, but it is rational if we give up to find better way. Meanwhile, every words have their own unicode, and we need to translate the unicode from hexadecimal to decimal for the request of Fontforge.

## Second: conclude a braille standard
Structure of braille are following a standard, and we create a SVG file base on the international standard.

Standard of braille (one unit)
<svg version="1.1" width="685" height="1000" xmlns="http://www.w3.org/2000/svg">
    <circle cx="215" cy="250" r="75" fill="#000000" stroke="#000000"/>
    <circle cx="215" cy="500" r="75" fill="#000000" stroke="#000000"/>
    <circle cx="215" cy="750" r="75" fill="#000000" stroke="#000000"/>
    <circle cx="465" cy="250" r="75" fill="#000000" stroke="#000000"/>
    <circle cx="465" cy="500" r="75" fill="#000000" stroke="#000000"/>
    <circle cx="465" cy="750" r="75" fill="#000000" stroke="#000000"/>
</svg>

One Chinese words combine with three units, and that would be:

stanard of braille (one Chinese word)
<svg version="1.1" width="2055" height="1000" xmlns="http://www.w3.org/2000/svg">
    <circle cx="215" cy="250" r="75" fill="#000000" stroke="#000000"/>
    <circle cx="215" cy="500" r="75" fill="#000000" stroke="#000000"/>
    <circle cx="215" cy="750" r="75" fill="#000000" stroke="#000000"/>
    <circle cx="465" cy="250" r="75" fill="#000000" stroke="#000000"/>
    <circle cx="465" cy="500" r="75" fill="#000000" stroke="#000000"/>
    <circle cx="465" cy="750" r="75" fill="#000000" stroke="#000000"/>
    <circle cx="900" cy="250" r="75" fill="#000000" stroke="#000000"/>
    <circle cx="900" cy="500" r="75" fill="#000000" stroke="#000000"/>
    <circle cx="900" cy="750" r="75" fill="#000000" stroke="#000000"/>
    <circle cx="1150" cy="250" r="75" fill="#000000" stroke="#000000"/>
    <circle cx="1150" cy="500" r="75" fill="#000000" stroke="#000000"/>
    <circle cx="1150" cy="750" r="75" fill="#000000" stroke="#000000"/>
    <circle cx="1585" cy="250" r="75" fill="#000000" stroke="#000000"/>
    <circle cx="1585" cy="500" r="75" fill="#000000" stroke="#000000"/>
    <circle cx="1585" cy="750" r="75" fill="#000000" stroke="#000000"/>
    <circle cx="1835" cy="250" r="75" fill="#000000" stroke="#000000"/>
    <circle cx="1835" cy="500" r="75" fill="#000000" stroke="#000000"/>
    <circle cx="1835" cy="750" r="75" fill="#000000" stroke="#000000"/>
</svg>

Since Fontforge use different coordinate expression, so the braille of a Chinese word would be different. (see src/dot-data.csv)

Third: produce standard of each braille
There are total 79 different units of braille for combining all Chinese words in our table, so we need to list all standard of those different units. (see src/table-data.csv)

## Fourth: learn the structure of FontForge

## Fifth:generate and test
After auto write all command of the file, it is opened on FontForge, than generate it as ".otf" type.
Next step is install the "otf" file, and check 10 Chinese words randomly. Finally, those 10 words are matched with their pinyin, so the version "001.100" is completed.
